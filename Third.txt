Answers

3.A.Reverse-i-search is a feature in Linux terminals that allows you to quickly search through your command line history. 
It is activated by pressing Ctrl+R and then entering a search term. 
The terminal will then display results of the search while you type, allowing you to quickly find the command you're looking for.

B.The command to list out the hidden directories in Linux is:

ls -al

C.I am very comfortable using Linux-based OS such as Ubuntu. 
I have been using Linux and am comfortable enough with the command line and other tools to get around and accomplish most tasks.

D.Yes, I have used servers to host web applications in the past. I have set up Apache(apache-maven-3.8.6-bin.zip).